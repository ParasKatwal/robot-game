import { useState, useEffect, useMemo } from "react";

import Tank from "components/atoms/Tank";
import Star from "components/atoms/Star";
import Timer from "components/atoms/Timer";
import UserInput from "components/atoms/UserInput";
import LeaderBoard from "components/organisms/LeaderBoard";
import ControlButtons from "components/molecules/ControlButtons";
import { getRandomCoordinates } from "utils";

function Index() {
  const [tankPosition, setTankPosition] = useState([40, 40]);
  const [starPosition, setStarPosition] = useState(
    getRandomCoordinates(tankPosition)
  );
  const [direction, setDirection] = useState("UP");
  const [score, setScore] = useState(0);
  const [over, setOver] = useState(true);
  const [user, setUser] = useState("");
  const [boardLeaders, setBoardLeaders] = useState(
    JSON.parse(localStorage.getItem("BoardLeaders")) || []
  );

  const newDeg = useMemo(() => {
    switch (direction) {
      case "RIGHT":
        return 90;
      case "LEFT":
        return 270;
      case "DOWN":
        return 180;
      case "UP":
        return 0;
    }
  }, [direction]);

  const updateUser = (e) => {
    setUser(e.target.value);
  };

  const updateTankPosition = (value) => {
    setTankPosition(value);
  };

  const updateDirection = (value) => {
    setDirection(value);
  };

  const gameOver = () => {
    setOver(true);
    let newStats = [...boardLeaders, { name: user, score: score }];
    newStats.sort((a, b) => b.score - a.score);
    newStats.slice(0, 10);
    setBoardLeaders(newStats);
    localStorage.setItem("BoardLeaders", JSON.stringify(newStats));
  };

  const newGame = () => {
    setTankPosition([40, 40]);
    setStarPosition(getRandomCoordinates(tankPosition));
    setDirection("UP");
    setScore(0);
    setOver(false);
  };

  useEffect(() => {
    if (
      tankPosition[0] >= 100 ||
      tankPosition[1] >= 100 ||
      tankPosition[0] < 0 ||
      tankPosition[1] < 0
    ) {
      gameOver();
      return;
    }
    if (
      tankPosition[0] === starPosition[0] &&
      tankPosition[1] === starPosition[1]
    ) {
      setStarPosition(getRandomCoordinates(tankPosition));
      setScore(score + 1);
    }
  }, [tankPosition]);

  return (
    <div className="mt-20 flex mx-10">
      <div className="w-full">
        <div className="w-125 flex justify-between mx-auto mb-5">
          <p>Score: {score}</p>
          {!over && <Timer gameOver={gameOver} />}
        </div>
        <div className="h-125 w-125 relative border border-solid border-black mx-auto">
          {over ? (
            <div className="h-full w-full flex justify-center items-center flex-col">
              <UserInput user={user} updateUser={updateUser} />
              <h1 className="text-2xl mb-4">Your Score: {score}</h1>
              <button
                className="bg-black text-white px-6 py-2 text-4xl disabled:opacity-50 disabled:cursor-not-allowed"
                type="button"
                onClick={newGame}
                disabled={!user}
              >
                Start New Game
              </button>
            </div>
          ) : (
            <>
              <Tank tankPosition={tankPosition} newDeg={newDeg} />
              <Star starPosition={starPosition} />
            </>
          )}
        </div>
        {!over && (
          <ControlButtons
            newDeg={newDeg}
            direction={direction}
            tankPosition={tankPosition}
            updateTankPosition={updateTankPosition}
            updateDirection={updateDirection}
          />
        )}
      </div>
      <LeaderBoard boardLeaders={boardLeaders} />
    </div>
  );
}

export default Index;
