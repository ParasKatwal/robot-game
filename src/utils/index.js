export const getRandomCoordinates = (tankPosition = []) => {
	let min = 1;
	let max = 99;
	let x = Math.floor((Math.random()*(max-min+1)+min)/20)*20;
	let y =  Math.floor((Math.random()*(max-min+1)+min)/20)*20;

	if(x === tankPosition[0] && y === tankPosition[1]){
		[x, y] = getRandomCoordinates();
	};

	return [x,y];
}