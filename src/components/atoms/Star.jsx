import { memo } from 'react';

function Star({starPosition}) {
	return (
		<div 
			className="h-25 w-25 absolute p-2"
			style={{ top: `${starPosition[0]}%`, left: `${starPosition[1]}%` }}
		>
			<svg
				xmlns="http://www.w3.org/2000/svg"
				x="0"
				y="0"
				enableBackground="new 0 0 512 512"
				version="1.1"
				viewBox="0 0 512 512"
				xmlSpace="preserve"
			>
				<path
					fill="#FFCC67"
					d="M414.616 512L256.024 391.104 97.4 512l60.592-195.608L0 196.032h195.264L256.024 0l60.736 196.032H512l-157.968 120.36L414.616 512z"
				></path>
				<path
					fill="#FFF"
					d="M256.024 391.104L97.4 512l60.592-195.608L0 196.032h195.264L256.024 0"
					opacity="0.3"
				></path>
			</svg>
		</div>
	);
};
  
export default memo(Star);