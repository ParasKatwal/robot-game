import {useState, useEffect} from 'react';

function Timer({gameOver}) {
  const [count, setCount] = useState(60);

	useEffect(() => {
		const interval = setInterval(() => {
      setCount(count -1);
    }, 1000);
		return () => clearInterval(interval);
	}, [count]);

  useEffect(() => {
    if(count && count < 1){
      gameOver()
      setCount(0);
    };
  }, [count]);

  return (
    <p>Time: <span className="w-6 inline-block">{count}</span></p>
  );
};

export default Timer;