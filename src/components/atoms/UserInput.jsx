import { useEffect, useRef } from 'react';

function UserInput({user, updateUser}) {
	const inputReference = useRef(null);

  useEffect(() => {
    inputReference.current.focus();
  }, []);

  return (
    <div className="mb-6 flex flex-col items-center">
			<input 
				type="text"
				value={user} 
				placeholder="Enter player name" 
				className="border border-black outline-none text-2xl text-center p-2 rounded-lg placeholder:text-gray-300"
				ref={inputReference}
				onChange={(e) => updateUser(e)}
			/>
			<span className="text-xs mt-1 text-red-400">Note: Enter player name to the play game</span>
		</div>
  );
};

export default UserInput;