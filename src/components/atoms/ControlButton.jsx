function Button({text, clickAction}) {
  return (
    <button 
			className="border border-solid border-black h-20 w-20 text-4xl" 
			type="button"
			onClick={clickAction}
		>
			{text}
		</button>
  );
};

export default Button;