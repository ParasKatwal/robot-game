import {memo} from 'react';

import tankImg from 'assets/img/tank.png';

function Tank({tankPosition, newDeg}) {
  return (
    <div 
			className="h-25 w-25 absolute"
			style={{ top: `${tankPosition[0]}%`, left: `${tankPosition[1]}%`, transform: `rotate(${newDeg}deg)`}}
		>
			<img src={tankImg} alt="tank" className="w-full h-full p-2" />
		</div>
  );
};

export default memo(Tank);