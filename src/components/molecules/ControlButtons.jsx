import ControlButton from 'components/atoms/ControlButton';

function ControlButtons({
  direction, 
  tankPosition, 
  newDeg, 
  updateTankPosition, 
  updateDirection
}) {
  const moveTank = () => {
    let newPosition = [];

    switch (direction) {
      case 'RIGHT':
        newPosition = [tankPosition[0], tankPosition[1] + 20];
        break;
      case 'LEFT':
        newPosition = [tankPosition[0], tankPosition[1] - 20];
        break;
      case 'DOWN':
        newPosition = [tankPosition[0] + 20, tankPosition[1]];
        break;
      case 'UP':
        newPosition = [tankPosition[0] - 20, tankPosition[1]];
        break;
    }
    updateTankPosition(newPosition);
  };

  const rotateLeft = () => {
    let newDirection = null;

    switch (newDeg) {
      case 0:
        newDirection = "LEFT";
        break;
      case 90:
        newDirection = "UP";
        break;
      case 180:
        newDirection = "RIGHT";
        break;
      case 270:
        newDirection = "DOWN";
        break;
    };
    updateDirection(newDirection);
  };

  const rotateRight = () => {
    let newDirection = null;

    switch (newDeg) {
      case 0:
        newDirection = "RIGHT";
        break;
      case 90:
        newDirection = "DOWN";
        break;
      case 180:
        newDirection = "LEFT";
        break;
      case 270:
        newDirection = "UP";
        break;
    };
    updateDirection(newDirection);
  };

  return (
    <div className="w-125 flex justify-center mx-auto mt-10 gap-6">
      <ControlButton
        text="&#8634;"
        clickAction={rotateLeft}
      />
      <ControlButton
        text="&#x2B61;"
        clickAction={moveTank}
      />
      <ControlButton
        text="&#x21BB;"
        clickAction={rotateRight}
      />
    </div>
  );
};

export default ControlButtons;